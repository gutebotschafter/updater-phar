#!/usr/bin/env bash

cd ./app/
echo "Installing composer dependencies"
php composer.phar install
cd ..
php create-phar.php
