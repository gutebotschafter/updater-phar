#!/usr/bin/env bash

DOCKER_PHP=$(docker ps | grep _php_ | cut -d' ' -f1)

docker exec -it ${DOCKER_PHP} sh -c "cd /var/www/html && php updater.phar $1 $2"
