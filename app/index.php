<?php

if (php_sapi_name() != 'cli') {
    die("Can only run in the cli!");
}

error_reporting(E_ERROR | E_PARSE);

require __DIR__ . '/vendor/autoload.php';

use Updater\App;

$cli = new App();
$cli->run();
