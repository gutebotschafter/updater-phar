<?php

namespace Updater;

use splitbrain\phpcli\CLI;
use splitbrain\phpcli\Exception;
use splitbrain\phpcli\Options;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\DumpException;

class TableInfo extends CLI
{
    /** @var array */
    private $configuration;

    /** @var */
    private $environment;

    /** @var */
    private $apiResponse;

    /** @var */
    private $yamlConfigurationFile;

    /**
     * DbTableInfo constructor.
     * @param bool $autocatch
     * @param string $environment
     * @param array $configuration
     * @param string $yamlConfigurationFile
     */
    public function __construct(Bool $autocatch = true, String $environment = "", Array $configuration = [], $yamlConfigurationFile = "")
    {
        parent::__construct($autocatch);

        $this->environment = $environment;
        $this->configuration = $configuration;
        $this->apiResponse = new ApiResponse();
        $this->yamlConfigurationFile = $yamlConfigurationFile;
    }

    /**
     * Updates the tables from origin
     */
    public function run()
    {
        $this->info("Get database tables list from " . $this->environment);
        $response = $this->apiResponse->get(
            $this->configuration["environments"][$this->environment],
            "",
            $this->configuration["token"],
            "view"
        );

        if ($response["head"]["code"] == 200) {
            $this->configuration["database"]["tables"] = $response["head"]["data"];

            try {
                file_put_contents($this->yamlConfigurationFile, Yaml::dump($this->configuration, 6));
            } catch (DumpException $exception) {
                $this->critical("Can't convert data to yaml...");
                exit(0);
            }

            $this->success("Writing new yaml configuration file...");
        }

        $this->success("Nothing more to do...");
    }

    /**
     * Register options and arguments on the given $options object
     *
     * @param Options $options
     * @return void
     *
     * @throws Exception
     */
    protected function setup(Options $options)
    {
    }

    /**
     * Your main program
     *
     * Arguments and options have been parsed when this is run
     *
     * @param Options $options
     * @return void
     *
     * @throws Exception
     */
    protected function main(Options $options)
    {
    }
}
