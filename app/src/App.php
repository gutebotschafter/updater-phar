<?php /** @noinspection PhpUndefinedClassInspection */

namespace Updater;

use splitbrain\phpcli\CLI;
use splitbrain\phpcli\Options;
use Symfony\Component\Yaml\Yaml;

/**
 * Class App
 * @package Updater
 */
class App extends CLI
{
    /** @var array */
    private $configuration = [];

    /** @var string */
    private $yamlConfigurationFile = "";

    /**
     * Parses the configuration yaml
     */
    protected function getConfiguration()
    {
        $baseDir = "";
        $outerBaseDir = "";

        if (!\Phar::running()) {
            $baseDir = __DIR__ . "/../";
        } else {
            $outerBaseDir = dirname(\Phar::running(false));

            try {
                \Phar::mount("updater.yml", $outerBaseDir . "/updater.yml");
            } catch (\PharException $e) {
                $this->critical("Please create the updater.yml with the proper configuration.");
                exit(1);
            }
        }

        $this->yamlConfigurationFile = $baseDir . "updater.yml";
        $this->configuration = Yaml::parseFile($this->yamlConfigurationFile);

        if (!file_exists($this->yamlConfigurationFile)) {
            $this->critical("Can't load updater.yml");
            $this->success("Nothing more to do...");
            exit(1);
        }

        if (\Phar::running()) {
            try {
                \Phar::mount("updater-token.php", $outerBaseDir . "/updater-token.php");
            } catch (\PharException $e) {
                $this->critical("Please create the updater.yml with the proper configuration.");
                exit(1);
            }

            try {
                \Phar::mount("LocalConfiguration.php", $outerBaseDir . "/" .
                    $this->configuration["webroot"] . "/typo3conf/LocalConfiguration.php");
            } catch (\PharException $e) {
                $this->warning("No TYPO3 Configuration found...");
            }

            try {
                \Phar::mount("wp-config.php", $outerBaseDir . "/" .
                    $this->configuration["webroot"] . "/wp-config.php");
            } catch (\PharException $e) {
                $this->warning("No Wordpress Configuration found...");
            }
        }

        $token = include($baseDir . "updater-token.php");
        $typo3conf = include($baseDir . "LocalConfiguration.php");
        $wpconf = include($baseDir . "wp-config.php");

        $this->configuration = array_merge($this->configuration, [
            "token" => $token["token"]
        ]);

        $database = [];

        if ($typo3conf) {
            $this->success("TYPO3 Configuration loaded...");

            $db = $typo3conf["DB"]["Connections"]["Default"];

            $database = [
                "host" => $db["host"],
                "dbname" => $db["dbname"],
                "user" => $db["user"],
                "password" => $db["password"]
            ];
        }

        if ($wpconf) {
            $this->success("Wordpress Configuration loaded...");
            // @TODO For WP
        }

        if (!$typo3conf && !$wpconf) {
            $this->critical("No database connection configuration found.");
            exit(1);
        }

        $this->configuration["database"]["connection"] = $database;

        $this->success("Main Configuration loaded");
    }

    /**
     * Prints the logo
     */
    protected function printLogo()
    {
        echo "
          ________        __           __________        __                .__            _____  __                
         /  _____/ __ ___/  |_  ____   \______   \ _____/  |_  ______ ____ |  |__ _____ _/ ____\/  |_  ___________ 
        /   \  ___|  |  \   __\/ __ \   |    |  _//  _ \   __\/  ___// ___\|  |  \\__  \\   __\\   __\/ __ \_  __ \
        \    \_\  \  |  /|  | \  ___/   |    |   (  <_> )  |  \___ \\  \___|   Y  \/ __ \|  |   |  | \  ___/|  | \/
         \______  /____/ |__|  \___  >  |______  /\____/|__| /____  >\___  >___|  (____  /__|   |__|  \___  >__|   
                \/                 \/          \/                 \/     \/     \/     \/                 \/   
        \n";
    }

    /**
     * @param Options $options
     */
    protected function setup(Options $options)
    {
        $options->setHelp('A CLI updater command tool');
        $options->registerOption('version', 'print version', 'v');
        $options->registerOption('tables', 'update tables from production', 't');
        $options->registerOption('update', 'update from production', 'u');
    }

    /**
     * @param Options $options
     * @throws \Exception
     */
    protected function main(Options $options)
    {
        $loadtime = new LoadTime();

        if ($options->getOpt('version')) {
            $this->info('1.3.0');
        } else if ($options->getOpt("tables")) {
            $this->printLogo();

            $this->getConfiguration();
            $arguments = $options->getArgs();

            $viewer = new TableInfo(true, $arguments[0], $this->configuration, $this->yamlConfigurationFile);
            $viewer->run();
        } else if ($options->getOpt("update")) {
            $this->printLogo();

            $this->getConfiguration();
            $arguments = $options->getArgs();

            if (
                count($arguments) > 0 &&
                $arguments[0] !== "" &&
                array_key_exists($arguments[0], $this->configuration["environments"])
            ) {
                $this->info("Updating from " . $arguments[0] . "...");

                $replicator = new Replicator(true, $arguments[0], $this->configuration);
                $replicator->run();
            } else {
                $this->error("Missing or bad parameter... Available parameters: " .
                    implode(", ", array_keys($this->configuration["environments"])));
            }
        } else {
            echo $options->help();
        }

        unset($loadtime);
    }
}
