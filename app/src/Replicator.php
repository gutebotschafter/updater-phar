<?php

namespace Updater;

use mysqli;
use splitbrain\phpcli\CLI;
use splitbrain\phpcli\Exception;
use splitbrain\phpcli\Options;

/**
 * Class Replicator
 * @package Updater
 */
class Replicator extends CLI
{
    /** @var array */
    private $configuration;

    /** @var */
    private $environment;

    /** @var mysqli */
    private $mysqli;

    /** @var */
    private $apiResponse;

    /**
     * Runner constructor.
     *
     * @param bool $autocatch
     * @param string $environment
     * @param array $configuration
     * @throws \Exception
     */
    public function __construct($autocatch = true, $environment = "", $configuration = [])
    {
        parent::__construct($autocatch);

        $this->environment = $environment;
        $this->configuration = $configuration;
        $this->apiResponse = new ApiResponse();

        $db = $this->configuration["database"]["connection"];
        $this->mysqli = new mysqli(
            $db["host"], $db["user"], $db["password"], $db["dbname"]
        );

        if ($this->mysqli->connect_errno) {
            $this->critical($this->mysqli->connect_error);
            die;
        }

        $this->mysqli->query("SET NAMES 'utf8'");
    }

    /**
     * @param $str
     * @return bool
     */
    private function isBinary($str): bool
    {
        return preg_match('~[^\x20-\x7E\t\r\n]~', $str) > 0;
    }

    /**
     * Builds the insert string
     *
     * @param $table
     * @param $entries
     * @return string
     */
    public function buildSqlInsert($table, $entries): string
    {
        if (is_array($entries) && count($entries) > 0) {
            $fields = [];
            $values = [];
            $insert = "INSERT INTO " . $table . " ";

            foreach ($entries as $key => $value) {
                $fields[] = "`" . $key . "`";
                $values[] = is_numeric($value) || is_bool($value)
                    ? $value
                    : "'" . addslashes($value) . "'";
            }

            return $insert . "(" . implode(' , ', $fields) . ") VALUES (" .
                implode(' , ', $values) . ")";
        }

        return "";
    }

    /**
     * Runs all jobs
     */
    public function run()
    {
        $jobs = $this->configuration["database"]["tables"];
        $this->notice("Loaded " . count($jobs) . " jobs");
        $this->debug("---------------------------------");
        
        foreach ($jobs as $job) {
            $error = false;

            $this->info("Running job on table '" . $job . "'");
            $response = $this->apiResponse->get(
                $this->configuration["environments"][$this->environment],
                $job,
                $this->configuration["token"]
            );

            if ($response["code"] !== 200) {
                $this->error("Response from table " . $job . " failed...");
                continue;
            }

            $responseJobs = $response["head"]["data"][$job];
            $this->success("   Retrieving " . count($responseJobs) . " " .
                (count($responseJobs) === 1 ? "Entry" : "Entries") . "...");

            $this->mysqli->query("TRUNCATE " . $job);
            $this->success("   Truncate table '" . $job . "'");

            $i = 1;
            foreach ($responseJobs as $entry) {
                $this->notice("      Inserting Entry " . $i);
                $insert = $this->buildSqlInsert($job, $entry);

                $this->mysqli->query($insert);

                if ($this->mysqli->error) {
                    $this->error("      " . $this->mysqli->error . " on Entry " . $i);
                    $error = true;

                    break;
                }

                $i++;
            }

            if ($error) {
                $this->error("   An error has occured...");
            } else if (count($responseJobs) > 0) {
                $this->success("   All Entries inserted...");
            } else {
                $this->info("   Nothing to import...");
            }

            $this->debug("---------------------------------");
        }

        $this->success("Nothing more to do...");
    }

    /**
     * Register options and arguments on the given $options object
     *
     * @param Options $options
     * @return void
     *
     * @throws Exception
     */
    protected function setup(Options $options)
    {
    }

    /**
     * Your main program
     *
     * Arguments and options have been parsed when this is run
     *
     * @param Options $options
     * @return void
     *
     * @throws Exception
     */
    protected function main(Options $options)
    {
    }
}
