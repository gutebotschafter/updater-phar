<?php

namespace Updater;

use splitbrain\phpcli\CLI;
use splitbrain\phpcli\Exception;
use splitbrain\phpcli\Options;

/**
 * Class LoadTime
 * @package Updater
 */
class LoadTime extends CLI
{
    /** @var int|mixed */
    private $timeStart = 0;

    /** @var int */
    private $timeEnd = 0;

    /** @var int */
    private $time = 0;

    /**
     * LoadTime constructor.
     *
     * @param bool $autocatch
     */
    public function __construct($autocatch = true)
    {
        parent::__construct($autocatch);

        $this->timeStart = microtime(true);
    }

    /**
     * LoadTime destructor.
     */
    public function __destruct()
    {
        $this->timeEnd = microtime(true);
        $this->time = $this->timeEnd - $this->timeStart;

        $this->info("Total execution time in " . number_format($this->time, 2) . " seconds");
    }

    /**
     * Register options and arguments on the given $options object
     *
     * @param Options $options
     * @return void
     *
     * @throws Exception
     */
    protected function setup(Options $options)
    {
    }

    /**
     * Your main program
     *
     * Arguments and options have been parsed when this is run
     *
     * @param Options $options
     * @return void
     *
     * @throws Exception
     */
    protected function main(Options $options)
    {
    }
}
