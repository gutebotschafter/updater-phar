<?php

namespace Updater;

class ApiResponse
{
    /**
     * Returns the api response
     *
     * @param $host
     * @param string $fields
     * @param string $token
     * @param string $type
     * @return array
     */
    public function get($host, $fields = "", $token = "", $type = "sql"): array
    {
        $host = $host . "?fields=" . $fields . "&type=" . $type . "&token=" . $token;
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $host);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        $head = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        return [
            "head" => json_decode($head, true),
            "code" => $httpCode
        ];
    }
}
