<?php

$pharFile = 'updater.phar';

if (file_exists($pharFile)) {
    unlink($pharFile);
}

$p = new Phar($pharFile);
$p->buildFromDirectory('app/');
$p->setDefaultStub('index.php', '/index.php');

echo $pharFile . " successfully created";
