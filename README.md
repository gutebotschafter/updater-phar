# Updater Phar Package

## Table of Contents

  - [Build](#build)
  - [Configuration](#configuration)

## Build

Run `./setup.sh` in the command line to install all dependencies and create the phar file.

## Configuration

You can find an example configuration in `updater-default.yml`.

## Help

Execute `php updater.phar -h` for help.

## Usage

Copy the `updater.phar` and `updater-default.yml` (Rename it to `updater.yml` and update the configuration) in the Project Folder.

Now you can run the phar-file from cli like `php updater.phar`. It shows you the help section. To Update from Production System
you can execute `php updater.phar -u production`.
